import numpy as np
import csv
import string
from nltk.tokenize import TweetTokenizer
from nltk import PorterStemmer
from nltk.util import bigrams
import math
import pickle


def loadAndSave():
    data=[]
    with open('dataset.csv', 'r', encoding="latin-1") as csvfile:
        reader = csv.reader(csvfile, delimiter='\n', quotechar='|')
        count = 0
        for row in reader:
            count+=1
            if count == 1:
                continue
            temp=row[0].split(',',3)
            newRow=[temp[0], temp[1], temp[3].strip('\",'+string.whitespace)]
            data.append(newRow)
    data = np.array(data)
    np.save('dataset', data)
    return data

def saveDictionaries():
    tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
    stemmer = PorterStemmer()
    sentences = np.load('dataset.npy')[:,2]
    allUnigrams = set([])
    allBigrams = set([])
    count = 0
    for s in sentences:
        count+=1
        if (count % math.floor(len(sentences)/100)) == 0:
            print(count / len(sentences))
        tokens = [stemmer.stem(w) for w in tokenizer.tokenize(s)]
        allUnigrams.update(tokens)
        allBigrams.update(list(bigrams(tokens)))
    allUnigrams = np.array(list(allUnigrams))
    allBigrams = np.array(list(allBigrams))
    np.save('unigrams', allUnigrams)
    np.save('bigrams', allBigrams)

def saveDictionaries2():
    allUnigrams = np.load('unigrams.npy')
    allBigrams = np.load('bigrams.npy')
    unigramDict = {}
    for i in range(len(allUnigrams)):
        unigramDict[allUnigrams[i]] = i
    bigramDict = {}
    for i in range(len(allBigrams)):
        bigramDict[allBigrams[i][0] + ' ' + allBigrams[i][1]] = i

    pickle.dump(unigramDict, open('unigramDict.pickle', 'wb'))
    pickle.dump(bigramDict, open('bigramDict.pickle', 'wb'))

saveDictionaries2()
#loadAndSave()
