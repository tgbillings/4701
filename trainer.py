import numpy as np
import nltk
from nltk.tokenize import TweetTokenizer
from textblob.classifiers import NaiveBayesClassifier
from nltk import PorterStemmer
from nltk.util import bigrams
import pickle

def trainUnigramModel(i):
    with open('trainData%i.pickle' % i,'rb') as file:
        trainData = pickle.load(file)
        tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
        stemmer = PorterStemmer()
        unigramTokenizedData = [({stemmer.stem(w) : 1 for w in tokenizer.tokenize(s[0])}, s[1]) for s in trainData]
        classifier = nltk.NaiveBayesClassifier.train(unigramTokenizedData)
        with open('unigramTrainedModel%i.pickle' % i, 'wb') as file2:
            pickle.dump(classifier, file2)

def testUnigramModel(i):
    with open('unigramTrainedModel%i.pickle' % i,'rb') as file:
        classifier = pickle.load(file)
        tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
        stemmer = PorterStemmer()
        testData = pickle.load(open('validationData%i.pickle' % i, 'rb'))
        testDataSet = [({stemmer.stem(w) : 1 for w in tokenizer.tokenize(s[0])}, s[1]) for s in testData]
        print("Ready to test")
        print(nltk.classify.accuracy(classifier, testDataSet))

def trainBigramModel(i):
    with open('trainData%i.pickle' % i,'rb') as file:
        trainData = pickle.load(file)
        tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
        stemmer = PorterStemmer()
        bigramTokenizedData = [({gram : 1 for gram in bigrams([stemmer.stem(w) for w in tokenizer.tokenize(s[0])])}, s[1]) for s in trainData]
        classifier = nltk.NaiveBayesClassifier.train(bigramTokenizedData)
        with open('bigramTrainedModel%i.pickle' % i, 'wb') as file2:
            pickle.dump(classifier, file2)

def testBigramModel(i):
    with open('bigramTrainedModel%i.pickle' % i,'rb') as file:
        classifier = pickle.load(file)
        tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
        stemmer = PorterStemmer()
        testData = pickle.load(open('validationData%i.pickle' % i, 'rb'))
        testDataSet = [({gram : 1 for gram in bigrams([stemmer.stem(w) for w in tokenizer.tokenize(s[0])])}, s[1]) for s in testData]
        print("Ready to test")
        print(nltk.classify.accuracy(classifier, testDataSet))

#getData()
#trainAndSave()
testUnigramModel(0)
testUnigramModel(1)
testUnigramModel(2)
testUnigramModel(3)
testUnigramModel(4)
testUnigramModel(5)
testUnigramModel(6)
testUnigramModel(7)
testUnigramModel(8)
testUnigramModel(9)