import numpy as np
from math import floor
import pickle
from nltk.tokenize import TweetTokenizer
from nltk import PorterStemmer
from nltk.util import bigrams
from collections import Counter
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn import svm

tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
stemmer = PorterStemmer()

def tokenize(s):
    return [stemmer.stem(w) for w in tokenizer.tokenize(s)]

def trainClassifier(i=-1, nGrams=1, useTfIdf=True):
    trainData=None
    corpus=None
    labels=None
    if i < 0:
        trainData = np.load('datasets/dataset.npy')
        corpus = trainData[:,2]
        labels = trainData[:,1]
    else:
        trainData = pickle.load(open('datasets/trainData%i.pickle' % i,'rb'))
        corpus = [row[0] for row in trainData]
        labels = [row[1] for row in trainData]
    
    vectorizer = HashingVectorizer(tokenizer=tokenize, ngram_range=(1, nGrams))
    corpusVector = vectorizer.fit_transform(corpus)
    #pickle.dump(vectorizer, open('svmdata/hVectorizer%i%i' % (nGrams,i), 'wb'))
    if useTfIdf:
        transformer = TfidfTransformer()  
        corpusVector = transformer.fit_transform(corpusVector) 
        pickle.dump(transformer, open('svmdata/tfidfTransformer%i%i' % (nGrams,i), 'wb'))
    clf = svm.LinearSVC()
    clf.fit(corpusVector, labels)
    pickle.dump(clf, open('svmdata/svmModel%i%i%s.pickle' % (nGrams, i, ('_raw', '')[useTfIdf]),'wb'))

def testClassifier(i, nGrams, useTfIdf=True):
    testData = pickle.load(open('datasets/validationData%i.pickle' % i,'rb'))
    testSentences = [row[0] for row in testData]
    vectorizer = pickle.load(open('svmdata/hVectorizer%i%i' % (nGrams, i), 'rb'))
    testVectors = vectorizer.transform(testSentences)
    if useTfIdf:
        transformer = pickle.load(open('svmdata/tfidfTransformer%i%i' % (nGrams, i), 'rb'))
        testVectors = transformer.transform(testVectors)
    clf = pickle.load(open('svmdata/svmModel%i%i%s.pickle' % (nGrams, i, ('_raw', '')[useTfIdf]),'rb'))
    labels = [row[1] for row in testData]
    print(clf.score(testVectors, labels))

# trainClassifier(-1, 1)
# for i in range(0,10):
#     print("starting %i" % i)
#     testClassifier(i,2, False)
