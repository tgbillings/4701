from TwitterSearch import *
import sys
import json
import base64
import numpy as np
import nltk
from nltk.tokenize import TweetTokenizer
from textblob.classifiers import NaiveBayesClassifier
from sklearn.feature_extraction.text import HashingVectorizer
from nltk.tokenize import TweetTokenizer
from nltk import PorterStemmer
from nltk.util import bigrams
from sklearn import svm
from nltk.util import bigrams
import pickle


classifier = pickle.load(open('svmdata/svmModel20_raw.pickle','rb'))
vectorizer = pickle.load(open('svmdata/hVectorizer20', 'rb'))

def getTopTweets(keywords, number):
    output = []
    try:
        tso = TwitterSearchOrder()
        tso.set_result_type("mixed")
        tso.set_keywords(keywords)
        tso.set_language('en')
        tso.set_include_entities(True) 
        tso.set_count(number)

        ts = TwitterSearch(
            consumer_key = 'lc6M0nZZfuldWpGbsd3vrEAf0',
            consumer_secret = 'ox0uQxLrFlyJrofHsEG5JCw1cc2FrAG6JWZJ45n4O5MCVgrgKo',
            access_token = '2772107492-tSirmalkIujbgNFYWwWPaaroG7T805Sr37KPn9e',
            access_token_secret = 'uNjz64kA0YfsJcQF30pAAtoGoRNNxIbI0MzqY3P5EjJ1N'
         )
        for tweet in ts.search_tweets_iterable(tso):
            if len(output)<number:
                output.append(tweet)
            else:
                break
        return output

    except TwitterSearchException as e:
        print(e)
        return output

def getOpinion(keywords, number):
    tweets = getTopTweets(keywords, number)
    tokenizer = TweetTokenizer(preserve_case=False, reduce_len=True, strip_handles=True)
    stemmer = PorterStemmer()
    uniqueTweets = list(set([s['text'] for s in tweets]))
    testVectors = vectorizer.transform(uniqueTweets)
    posSum = 0
    negSum = 0
    i = 0
    for tweetFeature in testVectors:
        output = classifier.predict(tweetFeature)
        print("%s :    %s" % (output, uniqueTweets[i]))
        i += 1
        if output[0] == 'pos':
            posSum += 1
        else:
            negSum +=1

    print("Pos: %.4f,  Neg: %.4f" % (posSum/ (posSum+negSum), negSum/(posSum+negSum)))
    return (posSum/ (posSum+negSum), negSum/ (posSum+negSum), uniqueTweets)
