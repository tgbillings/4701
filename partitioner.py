import numpy as np
from math import floor
import pickle

def saveData():
    data = np.load('dataset.npy')
    rows,cols = data.shape
    for i in range(10):
        startIndex = i*floor(rows/10)
        endIndex = (i+1)*floor(rows/10)
        validationData = data[startIndex:endIndex, :]
        trainData1 = data[0:startIndex, :]
        trainData2 = data[endIndex:, :]
        trainData = np.concatenate((trainData1, trainData2), axis=0)
        with open('trainData%i.pickle' % i,'wb') as file:
            object = [(row[2], ('neg', 'pos')[row[1]=='1']) for row in trainData]
            pickle.dump(object,file)
        print("Saved train data %i" % i)
        with open('validationData%i.pickle' % i,'wb') as file:
            object = [(row[2], ('neg', 'pos')[row[1]=='1']) for row in validationData]
            pickle.dump(object,file)
        print("Saved validation data %i" % i)

saveData()