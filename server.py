import threading
import webbrowser
import http.server
import urllib.parse
import svmModeler
from svmModeler import tokenize
import tweetFetcher
import json

FILE = 'frontend.html'
PORT = 8080

#http://stackoverflow.com/questions/336866/how-to-implement-a-minimal-server-for-ajax-in-python
class TestHandler(http.server.SimpleHTTPRequestHandler):
    """The test example handler."""

    def do_GET(self):
        path = urllib.parse.unquote(self.path)
        if path.startswith("/stats.json"):
            hashtags=path[12:].split('&')
            print(hashtags)
            (posScore, negScore, tweets) = tweetFetcher.getOpinion(hashtags, 100)
            self.send_response(200)
            self.send_header('Content-Type', 'application/json')
            self.end_headers()
            output = {}
            output['posScore'] = posScore
            output['negScore'] = negScore
            output['tweets'] = tweets
            self.wfile.write(json.dumps(output).encode())
            
        else:
            super(TestHandler, self).do_GET()

    def do_POST(self):
        """Handle a post request by returning the square of the number."""
        length = int(self.headers.getheader('content-length'))        
        data_string = self.rfile.read(length)
        try:
            result = int(data_string) ** 2
        except:
            result = 'error'
        self.wfile.write(result)


def open_browser():
    """Start a browser after waiting for half a second."""
    def _open_browser():
        webbrowser.open('http://localhost:%s/%s' % (PORT, FILE))
    thread = threading.Timer(0.5, _open_browser)
    thread.start()

def start_server():
    """Start the server."""
    server_address = ("", PORT)
    server = http.server.HTTPServer(server_address, TestHandler)
    server.serve_forever()

if __name__ == "__main__":
    #open_browser()
    start_server()